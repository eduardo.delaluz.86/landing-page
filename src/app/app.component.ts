import { Component } from '@angular/core';

@Component({
  selector: 'cle-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'landing-page';
}
